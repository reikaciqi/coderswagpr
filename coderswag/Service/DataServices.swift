//
//  DataServices.swift
//  coderswag
//
//  Created by Rei on 10/9/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation
class DataService {
    static let instance = DataService()
    
    private let categories = [
        
        Category(title: "SHIRTS", imageName: "shirts.png"),
        Category(title: "HOODIES", imageName: "hoodies.png"),
        Category(title: "HATS", imageName: "hats.png"),
        Category( title: "DIGITAL" , imageName: "digital.png")
    
]
    
    
    
    func getCategories() -> [Category]{
        return categories
    }
    
    
    private let hats = [
        Product(title: "title hat 1", price: "25", imageName: "hat01.jpg" ),
        Product(title: "title hat 2", price: "25", imageName: "hat02.jpg" ),
        Product(title: "title hat 3", price: "25", imageName: "hat03.jpg" ),
        Product(title: "title hat 4", price: "25", imageName: "hat04.jpg" )
    ]
    
    private let hoodies = [
        Product(title: "title hoodie 1", price: "25", imageName: "hoodie01.jpg" ),
        Product(title: "title hoodie 2", price: "25", imageName: "hoodie02.jpg" ),
        Product(title: "title hoodie 3", price: "25", imageName: "hoodie03.jpg" ),
        Product(title: "title hoodie 4", price: "25", imageName: "hoodie04.jpg" )
    ]

    private let shirts = [
        Product(title: "title shirt 1", price: "25", imageName: "shirt01.jpg" ),
        Product(title: "title shirt 2", price: "25", imageName: "shirt02.jpg" ),
        Product(title: "title shirt 3", price: "25", imageName: "shirt03.jpg" ),
        Product(title: "title shirt 4", price: "25", imageName: "shirt04.jpg" )
    ]
    
    
  
    
    
    
    func getProducts(forCategoryTitle title: String) -> [Product]  {
        switch title{
        case "SHIRTS":
            return getShirts()
        case "HOODIES":
            return getHoodies()
        case "HATS":
            return getHats()
      
            // nese klikohet digitals, do shkoje by default tek bluzat
        default:
        return getShirts()
        }
        
      
        
        
        
    }
    
    
       func getHats() -> [Product]{
             return hats
         }
    
    func getShirts() -> [Product]{
        return shirts
    }
    
    func getHoodies() -> [Product]{
        return hoodies
    }
  // nuk ka imazhe per digitals
    
}

