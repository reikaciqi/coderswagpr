//
//  Category.swift
//  coderswag
//
//  Created by Rei on 10/9/19.
//  Copyright © 2019 Rei. All rights reserved.
//

import Foundation


struct Category {
    private(set) public var title: String
    private(set) public var imageName: String
    
    init(title: String , imageName: String){
        self.title = title
        self.imageName = imageName
    }
}

